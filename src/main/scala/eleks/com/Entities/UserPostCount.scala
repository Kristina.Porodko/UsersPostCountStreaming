package eleks.com.Entities

case class UserPostCount(var name: String,
                         var surname: String,
                         var repostCount: Long) {
  def + (userPostCount: UserPostCount) : UserPostCount = {
    UserPostCount(this.name,
      this.surname,
      this.repostCount + userPostCount.repostCount )
  }
}


