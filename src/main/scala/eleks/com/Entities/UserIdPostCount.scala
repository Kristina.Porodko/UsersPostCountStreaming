package eleks.com.Entities

case class UserIdPostCount(userId: Int, var postCount: Long){

  def + (userIdPostCount: UserIdPostCount): UserIdPostCount = {
    UserIdPostCount(this.userId,
      this.postCount + userIdPostCount.postCount)
  }
}
