package eleks.com.Entities

case class User (id : Int, name : String, surname : String, city : String, country : String )
case class Post(postid : Int, userid : Int, content : String)
case class Action(userid : Int, postid : Int, action : String)

