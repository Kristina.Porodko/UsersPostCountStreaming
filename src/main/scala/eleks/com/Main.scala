package eleks.com

import eleks.com.Serializers.AvroSerde

import java.util.Properties

import org.apache.kafka.streams.StreamsConfig

object Main {
  def main(args: Array[String]): Unit = {
    val properties = configureStreamerProps()
    UserPostCountStatistics.configure(properties)

    val thread = new Thread(UserPostCountStatistics)
    thread.run()

  }

  private def configureStreamerProps(): Properties ={
    val streamProps = new Properties()
    streamProps.put(StreamsConfig.APPLICATION_ID_CONFIG, "UsersPostCountStreaming")
    streamProps.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG,  "localhost:9092")
    streamProps.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, classOf[AvroSerde])
    streamProps.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG,  classOf[AvroSerde])

    streamProps
  }

}
