package eleks.com


import org.apache.avro.generic.GenericRecord
import com.sksamuel.avro4s.RecordFormat
import org.apache.kafka.streams.{KafkaStreams, KeyValue}
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig
import org.apache.kafka.streams._
import org.apache.kafka.streams.kstream._

import scala.collection.mutable
import java.util.Properties
import java.util.concurrent.TimeUnit

import eleks.com.Entities.{Post, User, UserIdPostCount, UserPostCount}

object UserPostCountStatistics extends Runnable{
   var streams: KafkaStreams = null
   val streamBuilder: KStreamBuilder = new KStreamBuilder()


  val inputFormatter: RecordFormat[Post] = RecordFormat[Post]
  val innerFormatter: RecordFormat[UserIdPostCount] = RecordFormat[UserIdPostCount]
  val outputFormatter: RecordFormat[UserPostCount] = RecordFormat[UserPostCount]
  val userFormatter: RecordFormat[User] = RecordFormat[User]

  //   def transform(stream: KStream[Int, GenericRecord], usersTable: KTable[Int, GenericRecord]) : KStream[Int, GenericRecord] = {
  //
  //    stream.map[Int, GenericRecord](new UserPostCountKeyValueMapper)
  //        .groupByKey()
  //      .aggregate[mutable.Map[Int, UserIdPostCount]](
  //            new UserIdPostCountInitializer,
  //            new UserPostCountAggregator,
  //            new JSONSerde[mutable.Map[Int, UserIdPostCount]]
  //      ).join[GenericRecord, UserPostCount](usersTable, new UserJoiner)
  //      .toStream()
  //          .map[Int, GenericRecord](new UserPostCountRecord)
  //  }

  //  class UserJoiner extends ValueJoiner[mutable.Map[Int, UserIdPostCount], GenericRecord, UserPostCount]{
  //    override def apply(userIdPostCountMap: mutable.Map[Int, UserIdPostCount], userRecord: GenericRecord): UserPostCount ={
  //      val user = userFormatter.from(userRecord)
  //      var userid = user.id
  //      var postCount = userIdPostCountMap(userid).postCount
  //      return UserPostCount(user.name, user.surname, postCount)
  //    }
  //  }

  def configure(properties: Properties): Unit = {
    //TODO: take names of topics from some configs
    val usersTable = streamBuilder.table[Int, GenericRecord]("users")
    val postsStream: KStream[Int, GenericRecord] = streamBuilder.stream("posts")

    val outputStream: KStream[Int, GenericRecord] = transform(postsStream, usersTable)
    outputStream.to("usersPostCount")

    streams = new KafkaStreams(streamBuilder, properties)
  }

  def transform(postsStream: KStream[Int, GenericRecord], usersTable: KTable[Int, GenericRecord]) : KStream[Int, GenericRecord] = {
    val stream:  KStream[Int, GenericRecord] = postsStream.map[Int, GenericRecord](new UserPostCountKeyValueMapper)
      .groupByKey()
      .count()
      .toStream()
      .map[Int, java.lang.Long](new KeyValueMapper[Int, java.lang.Long, KeyValue[Int, java.lang.Long]]{
      override def apply(userId: Int, postCount: java.lang.Long): KeyValue[Int, java.lang.Long] = {
        return new KeyValue[Int, java.lang.Long](userId, postCount)
      }
    }).join[GenericRecord, GenericRecord](usersTable, new UserJoiner)

    stream
  }

  class UserJoiner extends ValueJoiner[java.lang.Long, GenericRecord, GenericRecord]{
    override def apply(postCount: java.lang.Long, userRecord: GenericRecord): GenericRecord = {
      var user = userFormatter.from(userRecord)
      var userPostCount = UserPostCount(user.name, user.surname, postCount)

      return outputFormatter.to(userPostCount)
    }
  }


  //  class StartWith extends Predicate[Int, GenericRecord] {
  //    override def test(key: Int, value: GenericRecord): Boolean = {
  //      val post = inputFormatter.from(value)
  //      return post.content.startsWith("Yes")
  //    }
  //  }

  // returns KeyValue(userId, post)
  class UserPostCountKeyValueMapper extends KeyValueMapper[Int, GenericRecord, KeyValue[Int, GenericRecord]] {
    override def apply(postId: Int, postRecord: GenericRecord) : KeyValue[Int, GenericRecord] = {
      val post = inputFormatter.from(postRecord)
      val userId = inputFormatter.from(postRecord).userid
      new KeyValue(userId, postRecord)
    }
  }

  class UserIdPostCountInitializer extends Initializer[mutable.Map[Int, UserIdPostCount]]{
    override def apply(): mutable.Map[Int, UserIdPostCount] = {
      mutable.Map()
    }
  }

  class UserPostCountAggregator extends Aggregator[Int, GenericRecord, mutable.Map[Int, UserIdPostCount]]{
    override def apply(userId: Int, postRecord: GenericRecord, aggregate: mutable.Map[Int, UserIdPostCount]) : mutable.Map[Int, UserIdPostCount] = {
      val post = inputFormatter.from(postRecord)

      if(aggregate.contains(userId)){
        aggregate(userId).postCount = aggregate(userId).postCount +1
      } else{
        val userIdPostCount_ = UserIdPostCount(userId, 1)
        aggregate(userId) = userIdPostCount_
      }

      aggregate
    }
  }

  // maps to final format!!! (name, surname, postCount)
  //  class UserPostCountRecord extends  KeyValueMapper[Int, mutable.Map[Int, UserPostCount], KeyValue[Int, GenericRecord]] {
  //    override def apply(userId: Int, userPostCountMap: mutable.Map[Int, UserPostCount] ): KeyValue[Int, GenericRecord] = {
  //      val outputValue = outputFormatter.to(userPostCountMap(userId))
  //      new KeyValue(userId, outputValue)
  //    }
  //  }

  class UserPostCountRecord extends  KeyValueMapper[Int, UserPostCount, KeyValue[Int, GenericRecord]] {
    override def apply(userId: Int, userIdPostCount: UserPostCount ): KeyValue[Int, GenericRecord] = {
      //val fakeUserPostCount = UserPostCount("fakename", "fakesurname", 15)
      val outputValue = outputFormatter.to(userIdPostCount)
      new KeyValue(userId, outputValue)
    }
  }

  override def run(){
    streams.start()

    Runtime.getRuntime.addShutdownHook(new Thread(new Runnable {
      override def run(): Unit = {
        streams.close(10, TimeUnit.SECONDS)
      }
    }))
  }
}
