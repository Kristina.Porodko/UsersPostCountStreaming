package eleks.com.Serializers

class AvroSerializer {

}

/*import java.io.{ByteArrayOutputStream, File}
import java.util

import org.apache.kafka.common.serialization.Serializer
import Entities.User
import com.sksamuel.avro4s.AvroOutputStream

import scala.util.parsing.json.JSON

class UserSerializator extends Serializer[User]{
  def Serialize(user : User): Unit ={
    val os = AvroOutputStream.data[User](new File("users.avro"))
    os.write(user)
    os.flush()
    os.close()
  }

  override def configure(configs: util.Map[String, _], isKey: Boolean) = ???

  override def serialize(topic: String, data: User) : Array[Byte]= {
//    val baos = new ByteArrayOutputStream()
//
//    val output = AvroOutputStream.binary[User](baos)
//    output.write(data)
//    output.close()
//    val bytes = baos.toByteArray()
//    bytes

  }

  override def close() = {

  }
} */

//
//import java.lang.reflect.{ParameterizedType, Type}
//import java.util
//
//import com.fasterxml.jackson.annotation.JsonInclude
//import com.fasterxml.jackson.core.JsonParseException
//import com.fasterxml.jackson.core.`type`.TypeReference
//import com.fasterxml.jackson.databind.ObjectMapper
//import com.fasterxml.jackson.databind.exc.{UnrecognizedPropertyException => UPE}
//import com.fasterxml.jackson.module.scala.DefaultScalaModule
//import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient
//import org.apache.kafka.common.serialization.{Deserializer, Serde, Serializer}
//
//object Json {
//
//  type ParseException = JsonParseException
//  type UnrecognizedPropertyException = UPE
//
//  private val mapper = new ObjectMapper()
//  mapper.registerModule(DefaultScalaModule)
//  mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
//
//  private def typeReference[T: Manifest] = new TypeReference[T] {
//    override def getType = typeFromManifest(manifest[T])
//  }
//
//  private def typeFromManifest(m: Manifest[_]): Type = {
//    if (m.typeArguments.isEmpty) {
//      m.runtimeClass
//    }
//    else new ParameterizedType {
//      def getRawType = m.runtimeClass
//
//      def getActualTypeArguments = m.typeArguments.map(typeFromManifest).toArray
//
//      def getOwnerType = null
//    }
//  }
//
//  object ByteArray {
//    def encode(value: Any): Array[Byte] = mapper.writeValueAsBytes(value)
//
//    def decode[T: Manifest](value: Array[Byte]): T =
//      mapper.readValue(value, typeReference[T])
//  }
//}
//
///**
//  * JSON serializer for JSON serde
//  *
//  * @tparam T
//  */
//class JSONSerializer[T] extends Serializer[T] {
//  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = ()
//
//  override def serialize(topic: String, data: T): Array[Byte] =
//    Json.ByteArray.encode(data)
//
//  override def close(): Unit = ()
//}
//
///**
//  * JSON deserializer for JSON serde
//  *
//  * @tparam T
//  */
//class JSONDeserializer[T >: Null <: Any : Manifest] extends Deserializer[T] {
//  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = ()
//
//  override def close(): Unit = ()
//
//  override def deserialize(topic: String, data: Array[Byte]): T = {
//    if (data == null) {
//      return null
//    } else {
//      Json.ByteArray.decode[T](data)
//    }
//  }
//}

import java.util
import io.confluent.kafka.serializers._
import org.apache.kafka.common.serialization._
import scala.collection.JavaConversions._

class AvroSerde extends Serde[AnyRef] {
  private var inner : Serde[AnyRef] = _

  {
    inner = Serdes.serdeFrom(new KafkaAvroSerializer(), new KafkaAvroDeserializer())
  }

  override def close(): Unit = {
    inner.serializer.close()
    inner.deserializer.close()
  }

  override def serializer: Serializer[AnyRef] = inner.serializer()

  override def deserializer: Deserializer[AnyRef] = inner.deserializer()


  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = {
    val convertedConfigs : util.Map[String, String] = new util.HashMap[String, String]

    //TODO : Configure url from the outside
    convertedConfigs.put("schema.registry.url", "http://localhost:8081")

    for(key <- configs.keySet().iterator()) {
      val value = configs.get(key).toString
      convertedConfigs.put(key, value)
    }

    inner.serializer.configure(convertedConfigs, isKey)
    inner.deserializer.configure(convertedConfigs, isKey)
  }
}





